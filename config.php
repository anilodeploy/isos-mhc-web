<?php
/* database config */ 

$cfg['db']['hostname'] = $_SERVER['MYSQL_HOST'];
$cfg['db']['username'] = $_SERVER['MYSQL_USER'];
$cfg['db']['password'] = $_SERVER['MYSQL_PASSWORD'];
$cfg['db']['database'] = $_SERVER['MYSQL_DATABASE'];
//$cfg['db']['port'] 	   = $_SERVER['MYSQL_PORT'];

//$cfg['db']['hostname'] = "45.62.248.192";
//$cfg['db']['username'] = "admuser";
//$cfg['db']['password'] = "admuser123";
//$cfg['db']['database'] = "newsos_db";

$cfg['db']['dbdriver'] = "mysqli";

/* module location HMVC */
$config['folder_modules']    = 'modules';
$config['modules_locations'] = array(
    getcwd().'/'.$config['folder_modules'].'/' => '../../'.$config['folder_modules'].'/',
);

$config['template_web']     = 'default';
$config['template_admin']   = 'atlant';

$config['mycript']          = true; 
$config['encryption_key']   = 'r3m4j4Id4m4n'; 

$config['activeLog']        = true;
$config['activeChat']       = false;

$config['base_url']         = "http://103.252.51.31/";
//$config['base_url']         = "http://".$_SERVER['SERVER_NAME']; 
//$config['domain']         = $_SERVER['SERVER_NAME'];
//$config['domain']           = 'hondacbrcommunity.com';
//$config['domain']         = 'sos.co.id';


$config['action_mask'] = array(
	"add" 		=> "Menambahkan",
	"edit"		=> "Mengupdate",
	"delete" 	=> "Menghapus"
);

$config['jenis_kelamin'] = array(
        'M' => "Laki-Laki",
        'F' => "Perempuan"
    );

$config['status_data'] = array(
        1 => "Aktif",
        0 => "Non Aktif"
    );

$config['target_url'] = array(
        '_blank' => "_Blank",
        'parent' => "Parent"
    );

$config['banner_position'] = array(
        'slider' => "Slider",
        'top' => "Top",
        'sidebar' => "Sidebar"
    );

$config['status_tampil'] = array(
        1 => "Ditampilkan",
        0 => "Tidak Ditampilkan"
    );

$config['site_type'] = array(
        2 => "Community",
        1 => "Utama"
    );
$config['template'] = array(
       // 'default'   => "Default",
        'lite'      => "Template Lite Kommunity"
    );

$config['type_static_content']  = array(
        'tentang-kami'          => 'Tentang Kami',
        'sejarah'               => 'Sejarah',
        'hubungi-kami'          => 'Hubungi Kami',
        'advertising'           => 'Adverstising',
        'disclaimer'            => 'Disclaimer',
        'privacy-policy'        => 'Privacy Policy',
        'term-of-use'           => 'Term Of Use'
    );

$config['sosmed'] = array(
        1 => array(
                "key"  => "fb",
                "name" => "facebook",
                "image"=> "facebook.png"
            ),
        2 => array(
                "key"  => "tw",
                "name" => "twitter",
                "image"=> "twitter.png"
            ),
        3 => array(
                "key"  => "ig",
                "name" => "instagram",
                "image"=> "instagram.png"
            )
    );

// upload path...
$config['upload_path']              = getcwd()."/assets/collections/";
$config['upload_path_klub']         = $config['upload_path']."/klub";
$config['upload_path_video']        = $config['upload_path']."/video";
$config['upload_path_user']         = $config['upload_path']."/user";
$config['upload_path_template']     = $config['upload_path']."/template";
$config['upload_path_content']      = $config['upload_path']."/content";
$config['upload_path_banner']       = $config['upload_path']."/banner";
$config['upload_path_photo']        = $config['upload_path']."/photo";
$config['upload_path_event']        = $config['upload_path']."/event";
$config['upload_path_kopdar']       = $config['upload_path']."/kopdar";
$config['upload_path_anggota_klub'] = $config['upload_path']."/anggota_klub";
$config['upload_path_member']       = $config['upload_path']."/member";
$config['upload_path_rank']         = $config['upload_path']."/rank";
$config['upload_path_badge']        = $config['upload_path']."/badge";
$config['upload_path_file']         = $config['upload_path']."/file";
